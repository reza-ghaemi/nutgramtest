<?php
/** @var SergiX44\Nutgram\Nutgram $bot */

use SergiX44\Nutgram\Nutgram;

/*
|--------------------------------------------------------------------------
| Nutgram Handlers
|--------------------------------------------------------------------------
|
| Here is where you can register telegram handlers for Nutgram. These
| handlers are loaded by the NutgramServiceProvider. Enjoy!
|
*/

$bot->onCommand('start', function (Nutgram $bot) {
    $bot->sendMessage('Hello, world!');
})->description('The start command!');

$bot->onCommand('google', function (Nutgram $bot) {
    $bot->sendMessage('https://www.google.com');
})->description('The start command!');

$bot->onCommand('mail', function (Nutgram $bot) {
    $bot->sendMessage('https://mail.google.com');
})->description('The start command!');

